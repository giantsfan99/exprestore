package net.pl3x.exprestore.exp;

public class PlayerExp {
	private Integer level;
	private Float exp;
	
	public PlayerExp(Integer level, Float exp) {
		this.level = level;
		this.exp = exp;
	}
	
	public Integer getLevel() {
		return level;
	}
	
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public Float getExp() {
		return exp;
	}
	
	public void setExp(Float exp) {
		this.exp = exp;
	}
}
