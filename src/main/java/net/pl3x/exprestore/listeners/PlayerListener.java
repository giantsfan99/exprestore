package net.pl3x.exprestore.listeners;

import java.util.HashMap;

import net.pl3x.exprestore.ExpRestore;
import net.pl3x.exprestore.exp.PlayerExp;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerListener implements Listener {
	private ExpRestore plugin;
	private HashMap<String,PlayerExp> savedExp = new HashMap<String,PlayerExp>();
	
	public PlayerListener(ExpRestore plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		player.setTotalExperience(0); // Reset so this doesnt ever get too high!
		PlayerExp pExp = new PlayerExp(player.getLevel(), player.getExp());
		savedExp.put(player.getName(), pExp);
		plugin.debug("Storing exp for " + player.getName() + "(Level: " + pExp.getLevel() + " expBar%: " + pExp.getExp() + ")");
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (savedExp.containsKey(player.getName())) {
			savedExp.remove(player.getName());
			plugin.debug("Removing stored exp for " + player.getName());
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerDeath(PlayerDeathEvent event) {
		event.setDroppedExp(0);
		Player player = event.getEntity();
		PlayerExp pExp = savedExp.get(player.getName());
		pExp.setLevel(player.getLevel());
		pExp.setExp(player.getExp());
		plugin.debug("Storing exp for " + player.getName() + "(Level: " + pExp.getLevel() + " expBar%: " + pExp.getExp() + ")");
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		player.setTotalExperience(0); // Reset so this doesnt ever get too high!
		if (!player.hasPermission("exprestore.keep"))
			return;
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			@Override
			public void run() {
				PlayerExp pExp = savedExp.get(player.getName());
				player.setLevel(pExp.getLevel());
				player.setExp(pExp.getExp());
				if (plugin.getConfig().getBoolean("notify-player", true))
					player.sendMessage(plugin.colorize(plugin.getConfig().getString("notify-message", "&dYour exprience levels have been restored.")));
				plugin.debug("Restoring exp for " + player.getName() + "(Level: " + pExp.getLevel() + " expBar%: " + pExp.getExp() + ")");
			}
		}, 1);
	}
}
